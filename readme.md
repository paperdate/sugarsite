@James look into this :
https://itnext.io/setting-up-a-sass-build-process-aa9fd92fa585
"scripts": {
"watch-sass": "node-sass sass/main.scss css/style.css --watch",
"compile-sass": "node-sass sass/main.scss css/style.comp.css",
"concat-css": "concat -o css/style.concat.css css/additional.css css/style.comp.css",
“prefix-css”: “postcss --use autoprefixer -b 'last 5 versions' css/style.concat.css -o css/style.prefix.css”
},

# Start

npm run start

# From project

xcode-select --install

npm install

gem install middleman

bundle install

middleman server

IF you have problems with "middleman server" try
bundle exec middleman server

# Build

///// middleman
middleman build

middleman build --verbose
