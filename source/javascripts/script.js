$(function() {
  console.log("ready!"),
    $('[data-nav="hamburger"]').click(function() {
      $('[data-nav="nav"]').toggleClass("show-mobile-main-nav"),
        $(this).toggleClass("show-mobile-hamburger"),
        $('[data-nav="hamburger"] svg').toggleClass("hamburger-active"),
        $("body").toggleClass("overflow-hidden");
    });
});
//
