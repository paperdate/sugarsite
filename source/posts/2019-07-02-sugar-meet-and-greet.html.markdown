---
title: What is a Sugar Meet and Greet?
subTitle: Sub title
site_description: Both the Potential Daddy (Pot) and Baby will agree on a time, location and upfront price to go on their first Sugar Meet+Greet...

# Featured Image
img: "/images/blog/sugar-meet-and-greet.jpg"
# author
author: Ashley
#
lastmod: 2019-06-25
priority: 0.8
layout: post
# Set featured to true to have it show in the side bar
featured: true
# excerpt
excerpt: "The Sugar Dating or Sugar relationship has been created by American Culture for over a decade but in recent years..."
---

## The spark of sugar: The Sugar Meet+Greet.

Both the Potential Daddy (Pot) and Baby will agree on a time, location and upfront price to go on their first Sugar Meet+Greet. The upfront price covers the cost of the sugar baby to pamper themselves and to arrive wherever is most convenient for the daddy. PaperDate makes setting up this initial meet and greet simple with our patent pending Sugar Meet+Greet system, you can propose the details of your meet and greet without wasting a dime.
