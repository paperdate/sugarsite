---
title: Why am I on the waiting list?
subTitle: Sub title
site_description: We often have questions about how our waiting list works.

# Featured Image
img: "/images/blog/why-am-i-on-the-waiting-list.jpg"
# author
author: Ashley
#
lastmod: 2019-06-25
priority: 0.6
layout: post
comments: false
# Set featured to true to have it show in the side bar
featured: true
# excerpt
excerpt: "We often have questions about how our waiting list works, so I wrote this blog to help you understand how everything works and give you a preview of how long it can take..."
---

## Quality requires analysis: why am I on the waiting list?

To ensure that the ratio of Sugar Daddies to Babies is balanced, we throttle the number of new babies that we allow in to the site. We are always looking at the volume of Babies and Daddies in each region before we accept new users, ensuring that everyone finds a pair without unfair competition.

One trick is to try joining in a smaller city near where you live, the ratio will be much lower and you will have a higher chance of being approved!

## Any forecast of how long I'll have to wait?

We would love to give you an exact time of when you'll be approved, but the deadline can vary a lot (we received more than 2,000 requests per day and we manually analyze each profile.

To improve your chances of being approved quicker, make sure that every profile field is finished and that you have uploaded the maximum number of photos. This will put you on top of the queue for your region's approval queue.
