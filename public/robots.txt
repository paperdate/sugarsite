User-agent: *
Disallow:
Disallow: /test/
Disallow: /version/
Disallow: /_start/

# Sitemap files
Sitemap: https://www.paperdate.io/sitemap.xml
